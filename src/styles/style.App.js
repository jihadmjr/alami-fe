import styled from 'styled-components';

export const DivApp = styled.div`
  font-family: Roboto, 'Helvetica Neue', Helvetica, Arial, 文泉驛正黑,
    'WenQuanYi Zen Hei', 'Hiragino Sans GB', '儷黑 Pro', 'LiHei Pro', 'Heiti TC',
    微軟正黑體, 'Microsoft JhengHei UI', 'Microsoft JhengHei', sans-serif;
  height: 100vh;
  justify-content: center;
  margin: 0 auto;
  width: 100vw;
  font-kerning: normal;
  word-spacing: normal;
  letter-spacing: normal;
  position: relative;
  color: rgba(0, 0, 0, 0.67);

  p,
  span,
  p span {
    user-select: none;
    font-kerning: normal;
    word-spacing: normal;
    letter-spacing: normal;
  }
`;
