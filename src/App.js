import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { DivApp } from './styles/style.App';
import Demo from './Container/Demo/Demo';
import Portfolio from './Container/Portfolio/Portfolio';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Sidebar />
      <DivApp>
        <Routes>
          <Route path='/' element={<Demo />} />
          <Route path='/portfolio' element={<Portfolio />} />
        </Routes>
      </DivApp>
    </BrowserRouter>
  );
}

export default App;
