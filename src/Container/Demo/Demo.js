import React from 'react';
import bannerImage from '../../assets/bannerImage.jpeg';
import bannerWave from '../../assets/wave.png';
import { COLOR, FONT_WEIGHT } from '../../constant/theme.constant';
import {
  DivBannerContainer,
  DivBannerContent,
  DivBannerContentDesc,
  DivBannerContentDetail,
  DivBannerContentTitle,
  DivBannerImage,
  DivBannerWave,
  DivDemoContainer,
  DivDemoContent,
  DivDemoText
} from './style.Demo';
import { DEMO_LOCALE } from '../../locale/text';

function Demo() {
  return (
    <DivDemoContainer>
      <DivBannerContainer>
        <DivBannerImage>
          <img
            draggable={false}
            src={bannerImage}
            alt='bannerimage'
            style={{ width: 'inherit', height: 'inherit', maxHeight: '70vh' }}
          />
        </DivBannerImage>
        <DivBannerWave>
          <img
            draggable={false}
            src={bannerWave}
            alt='bannerimage'
            width='100%'
          />
        </DivBannerWave>
        <DivBannerContent>
          <DivBannerContentTitle>
            {DEMO_LOCALE.BANNER_TITLE}
          </DivBannerContentTitle>
          <DivBannerContentDesc>{DEMO_LOCALE.BANNER_DESC}</DivBannerContentDesc>
          <DivBannerContentDetail>
            {DEMO_LOCALE.BANNER_DETAIL}
          </DivBannerContentDetail>
        </DivBannerContent>
      </DivBannerContainer>
      <DivDemoContent>
        <DivDemoText
          useitalic={true}
          color={COLOR.BLACK('67')}
          fontSize={'1.5rem'}
          fontWeight={FONT_WEIGHT.NORMAL}
        >
          {DEMO_LOCALE.DEMO_TITLE}
        </DivDemoText>
        <DivDemoText
          useitalic={false}
          textAlign='center'
          color={COLOR.GREEN}
          fontSize={'1.75rem'}
          fontWeight={FONT_WEIGHT.BOLD}
        >
          {DEMO_LOCALE.DEMO_DESCRIPTION}
        </DivDemoText>
        <DivDemoText
          useitalic={false}
          color={COLOR.BLACK('65')}
          fontSize={'0.75rem'}
          fontWeight={FONT_WEIGHT.MEDIUM}
          textAlign={'center'}
        >
          {DEMO_LOCALE.DEMO_DETAIL}
        </DivDemoText>
      </DivDemoContent>
    </DivDemoContainer>
  );
}

export default Demo;
