import styled from 'styled-components';
import { COLOR, FONT_WEIGHT } from '../../constant/theme.constant';

export const DivDemoContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  min-height: 100vh;
  margin: 0 auto;
`;

export const DivBannerContainer = styled.div`
  position: relative;
  @media (max-width: 900px) {
    position: relative;
    height: 50vh;
  }
`;

export const DivBannerImage = styled.div`
  width: 100%;
  @media (max-width: 900px) {
    width: 100%;
    height: 50vh;
  }
`;

export const DivBannerWave = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  transform: translateY(4px);
`;

export const DivDemoContent = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`;

export const DivDemoText = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.75rem 10%;
  text-align: ${props => props.textAlign || 'normal'};
  margin: ${props => props.margin || 'initial'};
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  color: ${props => props.color};
  font-style: ${props => (props.useitalic ? 'italic' : 'normal')};
  font-family: ${props => (props.useitalic ? 'cursive' : '')};
`;

export const DivBannerContent = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -70%);
  text-align: center;
  @media (max-width: 900px) {
    width: 80%;
  }
`;

export const DivBannerContentTitle = styled.div`
  padding: 5px 0;
  font-family: cursive;
  font-style: italic;
  color: ${COLOR.BLACK('90')};
  font-size: 1.5rem;
  @media (max-width: 900px) {
    font-size: 1rem;
  }
`;

export const DivBannerContentDesc = styled.div`
  padding: 5px 0;
  color: ${COLOR.BLACK('90')};
  font-weight: ${FONT_WEIGHT.BOLD};
  font-size: 2rem;
  @media (max-width: 900px) {
    font-size: 1.5rem;
  }
`;

export const DivBannerContentDetail = styled.div`
  padding: 8px 0 0;
  color: ${COLOR.BLACK('90')};
  font-weight: ${FONT_WEIGHT.BOLD};
  font-size: 0.75rem;
  @media (max-width: 900px) {
    font-size: 0.75rem;
  }
`;
