import React from 'react';
import Card from '../../components/Card';
import designImg from '../../assets/design.png';
import {
  ButtonPortfolio,
  DivButtonContainer,
  DivPortFolioServiceDesc,
  DivPortFolioServiceTitle,
  DivPortfolioContainer,
  DivPortfolioContent,
  DivPortfolioItem,
  DivPortfolioService,
  DivPortfolioSidebar
} from './style.Portfolio';
import { PORTFOLIO_LOCALE } from '../../locale/text';

function Portfolio() {
  function handleOpenResume() {
    window.open(
      'https://drive.google.com/file/d/1-uBILwGXdPoa6NM9t9_pzP7F0i1qmcQv/view?usp=sharing',
      '_blank'
    );
  }
  return (
    <DivPortfolioContainer>
      <DivPortfolioSidebar>My Portfolio</DivPortfolioSidebar>
      <DivPortfolioItem>
        <DivPortfolioContent>
          <Card
            icon={designImg}
            text={PORTFOLIO_LOCALE.DESIGN_CARD.title}
            desc={PORTFOLIO_LOCALE.DESIGN_CARD.desc}
          />
          <Card
            icon={designImg}
            text={PORTFOLIO_LOCALE.DESIGN_CARD.title}
            desc={PORTFOLIO_LOCALE.DESIGN_CARD.desc}
          />
          <Card
            icon={designImg}
            text={PORTFOLIO_LOCALE.DESIGN_CARD.title}
            desc={PORTFOLIO_LOCALE.DESIGN_CARD.desc}
          />
          <Card
            icon={designImg}
            text={PORTFOLIO_LOCALE.DESIGN_CARD.title}
            desc={PORTFOLIO_LOCALE.DESIGN_CARD.desc}
          />
          <Card
            icon={designImg}
            text={PORTFOLIO_LOCALE.DESIGN_CARD.title}
            desc={PORTFOLIO_LOCALE.DESIGN_CARD.desc}
          />
        </DivPortfolioContent>
        <DivPortfolioService>
          <DivPortFolioServiceTitle>Services</DivPortFolioServiceTitle>
          <DivPortFolioServiceDesc>
            {PORTFOLIO_LOCALE.SERVICE_TEXT.DESC}
          </DivPortFolioServiceDesc>
          <DivPortFolioServiceDesc>
            {PORTFOLIO_LOCALE.SERVICE_TEXT.DESC_CONT}{' '}
          </DivPortFolioServiceDesc>
          <DivButtonContainer>
            <ButtonPortfolio>
              <a
                href='https://drive.google.com/u/0/uc?id=1-uBILwGXdPoa6NM9t9_pzP7F0i1qmcQv&export=download'
                download
              >
                {PORTFOLIO_LOCALE.SERVICE_TEXT.BUTTON_DOWNLOAD_TEXT}
              </a>
            </ButtonPortfolio>
            <ButtonPortfolio onClick={handleOpenResume}>
              {PORTFOLIO_LOCALE.SERVICE_TEXT.BUTTON_PORTFOLIO}
            </ButtonPortfolio>
          </DivButtonContainer>
        </DivPortfolioService>
      </DivPortfolioItem>
    </DivPortfolioContainer>
  );
}

export default Portfolio;
