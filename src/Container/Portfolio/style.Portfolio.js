import styled from 'styled-components';
import { COLOR, FONT_WEIGHT } from '../../constant/theme.constant';

export const DivPortfolioContainer = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  min-height: 100%;
  margin: 0 auto;
`;

export const DivPortfolioSidebar = styled.div`
  font-family: cursive;
  color: ${COLOR.WHITE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  font-size: 1.5rem;
  background-color: gray;
  width: 30%;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;

  &:after {
    content: '';
    display: block;
    padding-bottom: 100%;
  }
`;

export const DivPortfolioItem = styled.div`
  display: flex;
  width: 100%;
  padding-top: 20px;
  justify-content: center;
  align-items: flex-start;
  background-color: #2f1b1b;
  @media (max-width: 900px) {
    flex-direction: column;
  }
`;

export const DivPortfolioContent = styled.div`
  width: 60%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  @media (max-width: 900px) {
    width: 100%;
  }
`;

export const DivPortfolioService = styled.div`
  width: 40%;
  color: ${COLOR.WHITE};
  @media (max-width: 900px) {
    padding: 12px;
    width: fit-content;
  }
`;

export const DivPortFolioServiceTitle = styled.div`
  font-weight: ${FONT_WEIGHT.BOLD};
`;
export const DivPortFolioServiceDesc = styled.div`
  padding-top: 12px;
`;

export const DivButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 12px;
`;

export const ButtonPortfolio = styled.div`
  border: 1px solid red;
  border-radius: 30px;
  font-size: 0.75rem;
  padding: 10px;
  margin: 0 12px;
  cursor: pointer;
`;
