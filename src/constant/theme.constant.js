export const COLOR = {
  BLACK: opacity => `rgba(0, 0, 0, 0.${opacity})`,
  GREEN: '#30B566',
  WHITE: '#FFFFFF',
  GRAY: '#F5F5F5'
};

export const FONT_WEIGHT = {
  NORMAL: '400',
  MEDIUM: '500',
  BOLD: '600'
};

export const PAGES = ['DEMOS', 'PAGES', 'PORTFOLIO'];
export const MENU = ['CART', 'VIDEO', 'PEOPLE', 'CHAT', 'DOCUMENT'];
