import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import VideocamOutlinedIcon from '@mui/icons-material/VideocamOutlined';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import InsertDriveFileOutlinedIcon from '@mui/icons-material/InsertDriveFileOutlined';
import { COLOR } from '../constant/theme.constant';

const Sidebar = () => {
  const [pageWidth, setPageWidth] = useState(window.innerWidth);

  useEffect(() => {
    function handleResize() {
      setPageWidth(window.innerWidth);
    }
    window.addEventListener('resize', handleResize);
  }, []);
  if (pageWidth <= 900) {
    return null;
  }
  return (
    <DivSideBarContainer>
      <DivSideBarIcon>
        <ShoppingCartOutlinedIcon sx={{ color: COLOR.BLACK('87') }} />
      </DivSideBarIcon>
      <DivLine />
      <DivSideBarIcon>
        <VideocamOutlinedIcon sx={{ color: COLOR.BLACK('87') }} />
      </DivSideBarIcon>
      <DivLine />
      <DivSideBarIcon>
        <PeopleAltOutlinedIcon sx={{ color: COLOR.BLACK('87') }} />
      </DivSideBarIcon>
      <DivLine />
      <DivSideBarIcon>
        <ChatBubbleOutlineOutlinedIcon sx={{ color: COLOR.BLACK('87') }} />
      </DivSideBarIcon>
      <DivLine />
      <DivSideBarIcon>
        <InsertDriveFileOutlinedIcon sx={{ color: COLOR.BLACK('87') }} />
      </DivSideBarIcon>
    </DivSideBarContainer>
  );
};

const DivSideBarContainer = styled.div`
  position: fixed;
  z-index: 2;
  top: 35%;
  right: 0;
  height: fit-content;
  background-color: ${COLOR.GRAY};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const DivSideBarIcon = styled.div`
  padding: 10px;
  cursor: pointer;
`;

const DivLine = styled.div`
  display: block;
  background-color: black;
  display: block;
  margin: 0;
  width: 100%;
  height: 1px;
`;

export default Sidebar;
