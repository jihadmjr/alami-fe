import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { COLOR, FONT_WEIGHT } from '../constant/theme.constant';

export default function BasicCard({ icon, text, desc }) {
  return (
    <Card
      style={{
        backgroundColor: COLOR.GRAY,
        margin: '7px',
        width: '250px'
      }}
    >
      <CardContent>
        <img src={icon} alt='card-icon' width={'50px'} />
        <Typography
          style={{ fontWeight: FONT_WEIGHT.BOLD }}
          sx={{ fontSize: 18 }}
          color='text.primary'
          gutterBottom
        >
          {text}
        </Typography>
        <Typography variant='body2'>{desc}</Typography>
      </CardContent>
    </Card>
  );
}
