export const DEMO_LOCALE = {
  BANNER_TITLE: 'Catering should be an experiene',
  BANNER_DESC: 'We use only the finest and freshest ingredients',
  BANNER_DETAIL:
    'At Sway catering we know that fodd is an important part of life. If the meal is not perfect. your event cannot be perfect.',
  DEMO_TITLE: 'Catering service in New York',
  DEMO_DESCRIPTION: 'We Specialize in corporate and private events',
  DEMO_DETAIL:
    'With 20 years of experience, we promise you freshest, local ingredients, hand-crafted cooking sprinkled with our unique whimsical elegance and exceptional service.'
};

export const PORTFOLIO_LOCALE = {
  DESIGN_CARD: {
    title: 'Design',
    desc: 'A full stack allaround designer that may or may not include a guide for specific creative people'
  },
  SERVICE_TEXT: {
    DESC: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    DESC_CONT:
      'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    BUTTON_DOWNLOAD_TEXT: 'Download CV',
    BUTTON_PORTFOLIO: 'Check My Portfolio'
  }
};
