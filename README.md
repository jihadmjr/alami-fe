# Alami FE Test

## How to run on development env

- `npm install`
- `npm start`
- navigate to `localhost:3000/` for open demos

## Deployment Step

- Use Vercel to deploy this ReactApp
- Need to build the project first using `npm run build`
- and serve the build folder to Vercel

- You access the project from this: https://alami-fe-jihadmjr.vercel.app/

## Library Used:

- Styled Components
- Material UI
